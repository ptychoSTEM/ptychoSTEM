%save images as tif files

if ptycho.varfunctions.show_synthetic_DF
    outimage=prep_matrix_for_imwrite(ptycho.DFimg);
    file='adf.tif';
    imwrite(outimage,file);
end

if ptycho.varfunctions.show_synthetic_ABF
    outimage=prep_matrix_for_imwrite(ptycho.ABFimg);
    file='abf.tif';
    imwrite(outimage,file);
end

if ptycho.varfunctions.single_side_band_reconstruction
    outimage=prep_matrix_for_imwrite(angle(ptycho.trotterimgR));
    file='ssb_r.tif';
    imwrite(outimage,file);
    
    outimage=prep_matrix_for_imwrite(angle(ptycho.trotterimgL));
    file='ssb_l.tif';
    imwrite(outimage,file);

     outimage=prep_matrix_for_imwrite(abs(ptycho.trotterimgL));
    file='ssb_amp.tif';
    imwrite(outimage,file);
end

if ptycho.varfunctions.wigner_distribution_deconvolution_reconstruction
    outimage=prep_matrix_for_imwrite(angle(ptycho.psi));
    file='wdd.tif';
    imwrite(outimage,file);
    
    outimage=prep_matrix_for_imwrite(abs(ptycho.psi));
    file='wdd_amp.tif';
    imwrite(outimage,file);
end

if ptycho.varfunctions.define_probe_function
    file=fopen('aberrations_corrected','w')
    
    fprintf(file,'C1    = \t%6.3f \tnm\n',(ptycho.aberr_input(1).*1e9));
    fprintf(file,'C12a  = \t%6.3f \tnm\n',(ptycho.aberr_input(2).*1e9));
    fprintf(file,'C12b  = \t%6.3f \tnm\n',(ptycho.aberr_input(3).*1e9));
    fprintf(file,'C23a  = \t%6.3f \tnm\n',(ptycho.aberr_input(4).*1e9));
    fprintf(file,'C23b  = \t%6.3f \tnm\n',(ptycho.aberr_input(5).*1e9));
    fprintf(file,'C21a  = \t%6.3f \tnm\n',(ptycho.aberr_input(6).*1e9));
    fprintf(file,'C21b  = \t%6.3f \tnm\n',(ptycho.aberr_input(7).*1e9));
    fprintf(file,'C3    = \t%6.3f \tum\n',(ptycho.aberr_input(8).*1e6));
    fprintf(file,'C34a  = \t%6.3f \tum\n',(ptycho.aberr_input(9).*1e6));
    fprintf(file,'C34b  = \t%6.3f \tum\n',(ptycho.aberr_input(10).*1e6));
    fprintf(file,'C32a  = \t%6.3f \tum\n',(ptycho.aberr_input(11).*1e6));
    fprintf(file,'C32b  = \t%6.3f \tum\n',(ptycho.aberr_input(12).*1e6));
    
    fclose(file);
end