function [ptycho] = show_synthetic_DPC(ptycho)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% To calculate and show synthethic DPC image. 
% Function requires structured array ptycho. Figures are displayed if
% ptycho.plot_figures is set to 1. 
% Input variables required are:
% ptycho.m: 4D array that contains the ronchigram for each probe position
% ptycho.R11: matrix of mask for DPC detector
% ptycho.R12: matrix of mask for DPC detector
% ptycho.R13: matrix of mask for DPC detector
% ptycho.R14: matrix of mask for DPC detector
% ptycho.R21: matrix of mask for DPC detector
% ptycho.R22: matrix of mask for DPC detector
% ptycho.R23: matrix of mask for DPC detector
% ptycho.R24: matrix of mask for DPC detector
% ptycho.R31: matrix of mask for DPC detector
% ptycho.R32: matrix of mask for DPC detector
% ptycho.R33: matrix of mask for DPC detector
% ptycho.R34: matrix of mask for DPC detector 
% % If ptycho.R11 is not available, it is assumed that the other DPC
% detector variables are not calculated as well. Then, they are calculated.
% This will require the following variables:
%   ptycho.ObjApt_angle: Probe convergence angle
%   ptycho.rot_angle: Rotation angle 
%   ptycho.tx: matrix of scattering angle in x from meshgrid
%   ptycho.ty: matrix of scattering angle in y from meshgrid
%   ptycho.theta: matrix of scattering angles
% Output variables added to ptycho are:
% ptycho.R11img: matrix of image for DPC detector
% ptycho.R12img: matrix of image for DPC detector
% ptycho.R13img: matrix of image for DPC detector
% ptycho.R14img: matrix of image for DPC detector
% ptycho.R21img: matrix of image for DPC detector
% ptycho.R22img: matrix of image for DPC detector
% ptycho.R23img: matrix of image for DPC detector
% ptycho.R24img: matrix of image for DPC detector
% ptycho.R31img: matrix of image for DPC detector
% ptycho.R32img: matrix of image for DPC detector
% ptycho.R33img: matrix of image for DPC detector
% ptycho.R34img: matrix of image for DPC detector
% ptycho.varfunctions.show_synthetic_DPC: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ptycho.varfunctions.show_synthetic_DPC %check if function has been run
    
    figure;
    subplot(3,2,1); imagesc(ptycho.R11img - ptycho.R13img); axis image; axis off;  title('DPC1-3')
    subplot(3,2,2); imagesc(ptycho.R12img - ptycho.R14img); axis image; axis off; title('DPC2-4')
    subplot(3,2,3); imagesc(ptycho.R21img - ptycho.R23img); axis image; axis off; title('DPC5-7')
    subplot(3,2,4); imagesc(ptycho.R22img - ptycho.R24img); axis image; axis off;  title('DPC6-8')
    subplot(3,2,5); imagesc(ptycho.R31img - ptycho.R33img); axis image; axis off; title('DPC9-10')
    subplot(3,2,6); imagesc(ptycho.R32img - ptycho.R34img); axis image; axis off;  title('DPC11-12')
    colormap gray
    
else
    if ptycho.varfunctions.calculate_detector_masks
        R11 = ptycho.R11;
        R12 = ptycho.R12;
        R13 = ptycho.R13;
        R14 = ptycho.R14;
        R21 = ptycho.R21;
        R22 = ptycho.R22;
        R23 = ptycho.R23;
        R24 = ptycho.R24;
        R31 = ptycho.R31;
        R32 = ptycho.R32;
        R33 = ptycho.R33;
        R34 = ptycho.R34;
    else
        ptycho = calculate_detector_masks(ptycho);
        R11 = ptycho.R11;
        R12 = ptycho.R12;
        R13 = ptycho.R13;
        R14 = ptycho.R14;
        R21 = ptycho.R21;
        R22 = ptycho.R22;
        R23 = ptycho.R23;
        R24 = ptycho.R24;
        R31 = ptycho.R31;
        R32 = ptycho.R32;
        R33 = ptycho.R33;
        R34 = ptycho.R34;
    end
    
    % calculate DPC images
    R11img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R12img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R13img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R14img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R21img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R22img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R23img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R24img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R31img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R32img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R33img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    R34img = zeros(size(ptycho.m,3),size(ptycho.m,4));
    
    for yy=1:size(ptycho.m,3)
        for xx = 1:size(ptycho.m,4)
            
            %DPC R1 to R8
            img = ptycho.m(:,:,yy,xx).*R11;
            R11img(yy,xx) = sum(img(:));
            img = ptycho.m(:,:,yy,xx).*R12;
            R12img(yy,xx) = sum(img(:));
            img = ptycho.m(:,:,yy,xx).*R13;
            R13img(yy,xx) = sum(img(:));
            img = ptycho.m(:,:,yy,xx).*R14;
            R14img(yy,xx) = sum(img(:));
            img = ptycho.m(:,:,yy,xx).*R21;
            R21img(yy,xx) = sum(img(:));
            img = ptycho.m(:,:,yy,xx).*R22;
            R22img(yy,xx) = sum(img(:));
            img = ptycho.m(:,:,yy,xx).*R23;
            R23img(yy,xx) = sum(img(:));
            img = ptycho.m(:,:,yy,xx).*R24;
            R24img(yy,xx) = sum(img(:));
            %DPC R9 to R12
            img = ptycho.m(:,:,yy,xx).*R31;
            R31img(yy,xx) = sum(img(:));
            img = ptycho.m(:,:,yy,xx).*R32;
            R32img(yy,xx) = sum(img(:));
            img = ptycho.m(:,:,yy,xx).*R33;
            R33img(yy,xx) = sum(img(:));
            img = ptycho.m(:,:,yy,xx).*R34;
            R34img(yy,xx) = sum(img(:));
            
        end
    end
    
    if ptycho.plot_figures
        
        figure; imagesc(R11+R12*3+R13*1+R14*2 + R21*3+R22*4+R23*3+R24*4 + R31*5+R32*6+R33*5+R34*6);
        axis image
        colormap jet
        
        figure;
        subplot(3,2,1); imagesc(R11img - R13img); axis image; axis off;  title('DPC1-3')
        subplot(3,2,2); imagesc(R12img - R14img); axis image; axis off; title('DPC2-4')
        subplot(3,2,3); imagesc(R21img - R23img); axis image; axis off; title('DPC5-7')
        subplot(3,2,4); imagesc(R22img - R24img); axis image; axis off;  title('DPC6-8')
        subplot(3,2,5); imagesc(R31img - R33img); axis image; axis off; title('DPC9-10')
        subplot(3,2,6); imagesc(R32img - R34img); axis image; axis off;  title('DPC11-12')
        colormap gray
    end
    
    ptycho.R11img = R11img;
    ptycho.R12img = R12img;
    ptycho.R13img = R13img;
    ptycho.R14img = R14img;
    ptycho.R21img = R21img;
    ptycho.R22img = R22img;
    ptycho.R23img = R23img;
    ptycho.R24img = R24img;
    ptycho.R31img = R31img;
    ptycho.R32img = R32img;
    ptycho.R33img = R33img;
    ptycho.R34img = R34img;
    ptycho.varfunctions.show_synthetic_DPC = 1;
end

end

