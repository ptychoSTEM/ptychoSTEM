function [ptycho] = show_trotters(ptycho,NumTrottersToDisplay)

if nargin<1
	error('input required: show_trotters(ptycho,NumTrottersToDisplay');
end

% this function needs comments

ObjSize = ptycho.ObjSize;
ObjApt_angle = ptycho.ObjApt_angle;
rot_angle = ptycho.rot_angle;
Q_x = ptycho.Q_x;
Q_y = ptycho.Q_y;
Q = ptycho.Q;
G_wp = ptycho.G_wp;
kx_wp = ptycho.kx_wp;
ky_wp = ptycho.ky_wp;

% rank the modulus of G from high to low values and obtain the high modulus Qp values

    if ptycho.varfunctions.calculate_G_power_spectrum_wrt_Kf
        psG = ptycho.psG;
    else
        ptycho = calculate_G_power_spectrum_wrt_Kf(ptycho);
        psG = ptycho.psG;
    end
    
    figure; imagesc(log10(1+abs(psG)));axis square; colormap jet;

    % calculate trotters from spatial frequencies that have the largest amplitude
    [~,locs] = sort(reshape(psG',1,[]),'descend');
  
% display results

    for i= (NumTrottersToDisplay*2) :-2:1

        yy = fix((locs(i)-1)/ size(G_wp,4)) + 1;
        xx = mod(locs(i)-1, size(G_wp,4))  + 1;

        if Q(yy,xx) > 0.1*ObjApt_angle

            g =  squeeze(G_wp(:,:,yy,xx));
            Q_x_rot = Q_x(xx).*cos(rot_angle) - Q_y(yy).*sin(rot_angle);
            Q_y_rot = Q_x(xx).*sin(rot_angle) + Q_y(yy).*cos(rot_angle); % + skew_k * Q_x_rot ;

            [dx,dy] = meshgrid(kx_wp + Q_x_rot , ky_wp + Q_y_rot);
            d1 = sqrt(dx.*dx+dy.*dy);
            [dx,dy] = meshgrid(kx_wp - Q_x_rot , ky_wp - Q_y_rot);
            d2 = sqrt(dx.*dx+dy.*dy);
            [dx,dy] = meshgrid(kx_wp, ky_wp);
            d3 = sqrt(dx.*dx+dy.*dy);

            g_ampL = abs(g);
            g_ampL(d1>ObjApt_angle)=0;
            g_ampL(d2<ObjApt_angle)=0;
            g_ampL(d3>ObjApt_angle)=0;
            g_ampR = abs(g);
            g_ampR(d1<ObjApt_angle)=0;
            g_ampR(d2>ObjApt_angle)=0;
            g_ampR(d3>ObjApt_angle)=0;
            g_amp = g_ampR + g_ampL;

            if yy==fix(ObjSize(1)/2)+1 && xx==fix(ObjSize(2)/2)+1
                g_amp = abs(g);
                g_ampL = abs(g);
                g_ampR = abs(g);
            end

            g_phaseL = angle(g);
            g_phaseL(d1>ObjApt_angle)=0;
            g_phaseL(d2<ObjApt_angle)=0;
            g_phaseL(d3>ObjApt_angle)=0;
            g_phaseR = angle(g);
            g_phaseR(d1<ObjApt_angle)=0;
            g_phaseR(d2>ObjApt_angle)=0;
            g_phaseR(d3>ObjApt_angle)=0;
            g_phase = g_phaseL + g_phaseR;

            figure;
            subplot(2,2,1)
            imagesc(abs(g)); axis square ;
            set(gca,'YTick',[]); set(gca,'XTick',[]);
            colorbar;
            title(['[',num2str(yy),'/',num2str(xx),']','Q=',num2str(Q(yy,xx)*1000),'mrad']);
            subplot(2,2,2)
            imagesc(angle(g)); axis square ;
            set(gca,'YTick',[]); set(gca,'XTick',[]);
            colorbar;
            subplot(2,2,3)
            imagesc(g_amp); axis square;
            set(gca,'YTick',[]); set(gca,'XTick',[]);
            colorbar;
            subplot(2,2,4)
            imagesc(g_phase); axis square ;
            set(gca,'YTick',[]); set(gca,'XTick',[]);
            colorbar;

        end
    end
   
    ptycho.psG = psG;
    ptycho.locs = locs;
    
end
